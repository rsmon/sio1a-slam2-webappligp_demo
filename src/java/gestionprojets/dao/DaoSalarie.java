
package gestionprojets.dao;

import gestionprojets.entites.Salarie;
import java.util.List;

public interface DaoSalarie {
    
    List<Salarie> getTousLesSalaries();  
    Salarie       getSalarieNumero(Long pId);
}


