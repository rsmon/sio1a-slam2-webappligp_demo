
package gestionprojets.dao;

import gestionprojets.entites.Salarie;
import gestionprojets.persistance.Persistance;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class DaoSalarieORM implements DaoSalarie {
              
   @Inject private Persistance pe;
    
   @Override
   public List<Salarie>    getTousLesSalaries() {
        
        return   pe.getEm().createQuery("Select s from Salarie s").getResultList();
   }

   @Override
   public Salarie           getSalarieNumero(Long pId){
        
        return   pe.getEm().find(Salarie.class, pId);
   }    
}
