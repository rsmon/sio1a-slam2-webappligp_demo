package gestionprojets.entites;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

@Entity
public class Projet implements Serializable {
        
    //<editor-fold defaultstate="collapsed" desc="Attributs privés">
    
    @Id
    private String codeProj;
    
    private String descProj;
   
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date   dateDebP;
    
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date   dateFinP;
    
    private Float  montantDevis;
    
    // Attributs navigationnels
    
    @ManyToOne
    @JoinColumn(name="NUMCLI")
    private Client leClient;
    
    @OneToMany(mappedBy = "leProjet")
    private List<Affectation> lesAffectations=new LinkedList();
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Getters et Setters">
    
    public String getCodeProj() {
        return codeProj;
    }
    
    public void setCodeProj(String codeProj) {
        this.codeProj = codeProj;
    }
    
    public String getDescProj() {
        return descProj;
    }
    
    public void setDescProj(String descProj) {
        this.descProj = descProj;
    }
    
    public Date getDateDebP() {
        return dateDebP;
    }
    
    public void setDateDebP(Date dateDebP) {
        this.dateDebP = dateDebP;
    }
    
    public Date getDateFinP() {
        return dateFinP;
    }
    
    public void setDateFinP(Date dateFinP) {
        this.dateFinP = dateFinP;
    }
    
    public Client getLeClient() {
        return leClient;
    }
    
    public void setLeClient(Client leClient) {
        this.leClient = leClient;
    }

    public Float getMontantDevis() {
        return montantDevis;
    }

    public void setMontantDevis(Float montantDevis) {
        this.montantDevis = montantDevis;
    }
    
    
    
    public List<Affectation> getLesAffectations() {
        return lesAffectations;
    }

    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Hascode et Equals">
    
    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + (this.codeProj != null ? this.codeProj.hashCode() : 0);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Projet other = (Projet) obj;
        if ((this.codeProj == null) ? (other.codeProj != null) : !this.codeProj.equals(other.codeProj)) {
            return false;
        }
        return true;
    }
    //</editor-fold>
    
}



