package gestionprojets.entites;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

@Entity
public class Affectation implements Serializable {
   
    //<editor-fold defaultstate="collapsed" desc="Attributs privés">
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date    dateaff;
    private Boolean pilote;

    //Attributs navigationnels
    
    @ManyToOne
    @JoinColumn(name="IDSAL")
    private Salarie leSalarie;
    
    @ManyToOne
    @JoinColumn(name="CODEPROJ")
    private Projet  leProjet;
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Getters et Setters">
    
    public Boolean getPilote() {
        return pilote;
    }

    public void    setPilote(Boolean pilote) {
        this.pilote = pilote;
    }

    public Date getDateaff() {
        return dateaff;
    }
    public void setDateaff(Date dateaff) {
        this.dateaff = dateaff;
    }

    public Projet getLeProjet() {
        return leProjet;
    }
    public void setLeProjet(Projet leProjet) {
        this.leProjet = leProjet;
    }

    public Salarie getLeSalarie() {
        return leSalarie;
    }
    public void setLeSalarie(Salarie leSalarie) {
        this.leSalarie = leSalarie;
    }
    
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Hascode et Equals">
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 11 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Affectation other = (Affectation) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
    
    //</editor-fold>
}
