package gestionprojets.entites;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Salarie implements Serializable {
    
    //<editor-fold defaultstate="collapsed" desc="Attributs privés">
     
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long   id;
    
    private String nom;
    private String prenom;
    private String sexe;
   
    @Temporal(TemporalType.DATE)
    private Date   dateNaiss;
    
    private Float  salaire;
    
    // Attributs navigationnels
    
    @ManyToOne
    @JoinColumn(name="CODEPOLE")
    private Pole lePole;
    
    @OneToMany(mappedBy = "leSalarie")
    private List<Affectation> lesAffectations=new LinkedList();
   
    //</editor-fold>
     
    //<editor-fold defaultstate="collapsed" desc="Getters et Setters">
      
    public Long getId() {
        return id;
    }
    
    public String getNom() {
        return nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
   
    public String getPrenom() {
        return prenom;
    }

    public String getSexe() {
        return sexe;
    }

    public Date getDateNaiss() {
        return dateNaiss;
    }

    public Float getSalaire() {
        return salaire;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public void setDateNaiss(Date dateNaiss) {
        this.dateNaiss = dateNaiss;
    }

    public void setSalaire(Float salaire) {
        this.salaire = salaire;
    }

    public Pole getLePole() {
        return lePole;
    }

    public void setLePole(Pole lePole) {
        this.lePole = lePole;
    }
    
    public List<Affectation> getLesAffectations() {
        return lesAffectations;
    }

    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="HashCode et Equals">
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Salarie other = (Salarie) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
    
    //</editor-fold>
}


