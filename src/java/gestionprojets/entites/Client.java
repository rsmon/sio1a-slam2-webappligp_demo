package gestionprojets.entites;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Client implements Serializable {

    //<editor-fold defaultstate="collapsed" desc="Attributs privés">
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long   numcli;
    
    private String nomcli; 
    private String adrcli;
    
    // Attribut navigationnel
    
    @OneToMany(mappedBy = "leClient")
    private List<Projet>   lesProjets= new LinkedList();
    
    //</editor-fold>
   
    //<editor-fold defaultstate="collapsed" desc="Getters et Setters">

    public List<Projet> getLesProjets() {
        return lesProjets;
    }

    public void setLesProjets(List<Projet> lesProjets) {
        this.lesProjets = lesProjets;
    }
     
     
     public Long getNumcli() {
         return numcli;
     }
     
     public void setNumcli(Long numcli) {
         this.numcli = numcli;
     }
     
     
     public String getNomcli() {
         return nomcli;
     }
     
     public void setNomcli(String nomcli) {
         this.nomcli = nomcli;
     }
     
     
    public String getAdrcli() {
        return adrcli;
    }

    public void setAdrcli(String adrcli) {
        this.adrcli = adrcli;
    }

    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Hashcode et Equals">
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + (this.numcli != null ? this.numcli.hashCode() : 0);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Client other = (Client) obj;
        if (this.numcli != other.numcli && (this.numcli == null || !this.numcli.equals(other.numcli))) {
            return false;
        }
        return true;
    }
    //</editor-fold>
    
}


