
package gestionprojets.entites;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Pole implements Serializable {
  
    //<editor-fold defaultstate="collapsed" desc="Attributs privés">
   
    @Id
    private String codePole;
    
    private String nomPole;
    
    // Attribut navigationnel
      
    @OneToMany(mappedBy = "lePole")
    private List<Salarie> lesSalaries = new LinkedList();;
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Getters et Setters">
    
    public String getCodePole() {
        return codePole;
    }
    
    public String getNomPole() {
        return nomPole;
    }
    
    public List<Salarie> getLesSalaries() {
        return lesSalaries;
    }
    
    public void setCodePole(String codePole) {
        this.codePole = codePole;
    }
    
    public void setNomPole(String nomPole) {
        this.nomPole = nomPole;
    }
    
    public void setLesSalaries(List<Salarie> lesSalaries) {
        this.lesSalaries = lesSalaries;
    }
    
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Hashcode et Equals ">
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (this.codePole != null ? this.codePole.hashCode() : 0);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pole other = (Pole) obj;
        if ((this.codePole == null) ? (other.codePole != null) : !this.codePole.equals(other.codePole)) {
            return false;
        }
        return true;
    }
    
    
    //</editor-fold>
}
