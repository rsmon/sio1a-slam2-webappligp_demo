
package gestionprojets.persistance;

import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

@Singleton
public class Persistance {
    
    private EntityManagerFactory emf;
    private EntityManager        em; 

    public Persistance() {
       
       emf= Persistence.createEntityManagerFactory("PU");
       em = emf.createEntityManager();       
    }
    
    public EntityManager getEm() { return em;}
}


