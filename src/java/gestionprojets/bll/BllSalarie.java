package gestionprojets.bll;

public interface BllSalarie {

    int   effectif();
    int   effectif(String pSexe);
    Float moyenneSalaires();
    Float moyenneSalaires(String pSexe);   
}
