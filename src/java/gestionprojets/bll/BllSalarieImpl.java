
package gestionprojets.bll;

import gestionprojets.dao.DaoSalarie;
import gestionprojets.entites.Salarie;
import javax.inject.Inject;

public class BllSalarieImpl implements BllSalarie {
      
    @Inject private  DaoSalarie daoSalarie;
  
    @Override
    public Float  moyenneSalaires(){
        
        Float totalSalaires=0f, moyenneSalaires=0f;
        int   nbSalaries=0;
        
        for (Salarie sal: daoSalarie.getTousLesSalaries()){
            
            totalSalaires+=sal.getSalaire(); nbSalaries++;
            
        }
        moyenneSalaires=totalSalaires/nbSalaries;
        
        return moyenneSalaires;
    }
    
    @Override
    public Float  moyenneSalaires(String pSexe){
        
        Float totalSalaires=0f, moyenneSalaires=0f;
        int   nbSalaries=0;
        
        for (Salarie sal: daoSalarie.getTousLesSalaries()){
            
            if( sal.getSexe().equals(pSexe)){
                
                totalSalaires+=sal.getSalaire(); nbSalaries++;
            }
        }
        moyenneSalaires=totalSalaires/nbSalaries;
        
        return moyenneSalaires;
    }
    
    @Override
    public int    effectif(){  return daoSalarie.getTousLesSalaries().size();}
    
    @Override
    public int    effectif(String pSexe){
        
        int nb=0;
        
        for (Salarie sal: daoSalarie.getTousLesSalaries()){
            
            if( sal.getSexe().equals(pSexe)){ nb++; }
        }
        
        return nb;
    }

}
