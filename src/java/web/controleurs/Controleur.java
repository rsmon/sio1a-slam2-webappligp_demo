package web.controleurs;

import gestionprojets.dao.DaoSalarie;
import gestionprojets.entites.Salarie;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@ManagedBean
public class Controleur {
     
   @Inject DaoSalarie daoSalarie;
   
   private Long       numsal;
   private Salarie    unSalarie;
      
   public void  rechercheSalarie(){ 
         
     unSalarie = daoSalarie.getSalarieNumero(numsal);
     traiterCasSalarieInexistant();     
   }
    
   private void traiterCasSalarieInexistant() {
       
        if( unSalarie==null) afficherMessage("Pas de salarié avec ce numéro");
    }
   
   private void afficherMessage( String message ){
    
        FacesContext contexte=FacesContext.getCurrentInstance();
        contexte.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, null, message));    
   }
   
   public void  razInfosDuFormulaire(){ unSalarie= new Salarie(); }
     
   //<editor-fold defaultstate="collapsed" desc="Getters et Setters">
     
     public Salarie getUnSalarie() {
         return unSalarie;
     }
     
     public void setUnSalarie(Salarie unSalarie) {
         this.unSalarie = unSalarie;
     }
      
    public Long getNumsal() {
        return numsal;
    }

    public void setNumsal(Long numsal) {
        this.numsal = numsal;
    }   
     //</editor-fold>

}


